package TransferFundsTests;

import Enums.DropDownsEnum;
import Enums.TransferFundsAccountTypesEnum;
import Models.TestAssertModel;
import Pages.AccountSummaryPage;
import Pages.HomePage;
import Pages.LoginPage;
import Pages.TransferFundsPage;
import Utils.BaseTestClass;
import Utils.ConfigurationConstants;
import Utils.Log;
import Utils.TestFlowUitls;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class verifyTransferFundsFromSavingsToLoanIsWorkingCorrectly extends BaseTestClass {

    private TransferFundsPage transferFundsPage;

    @Override
    protected void beforeClassExtended() throws IOException {
        transferFundsPage = TestFlowUitls
                .navigateToBaseUrlAndLogin(driver, baseUrl, ConfigurationConstants.USERNAME_COURSE, ConfigurationConstants.PASSWORD_COURSE)
                .navigateToTransferFunds();
    }

    @Test(priority = 0)
    public void checkIfRightValuesArePresentedInFromAccountDDL() {
        TestAssertModel testAssertModel = transferFundsPage.checkDDlContainsValues(DropDownsEnum.FROM_ACCOUNT, "Checking", "Loan", "Credit Card");
        Assert.assertTrue(testAssertModel.isPassed(), testAssertModel.getErrorMessage());
    }

    @Test(priority = 0)
    public void checkIfRightValuesArePresentedInToAccountDDL() {
        TestAssertModel testAssertModel = transferFundsPage.checkDDlContainsValues(DropDownsEnum.TO_ACCOUNT, "Checking", "Credit Card");
        Assert.assertTrue(testAssertModel.isPassed(), testAssertModel.getErrorMessage());
    }

    @Test(priority = 1)
    public void checkStateOneFromAccount() {
        transferFundsPage = transferFundsPage.insertAllValuesAndClickContinue(TransferFundsAccountTypesEnum.CHECKING_ACCOUNT, TransferFundsAccountTypesEnum.CREDIT_CARD_ACCOUNT,
                "300", "Pay check for the UI Automation Course");
        String inputText = transferFundsPage.readValueFromAccountFromField();
        Log.assertion("The from account input field should have value Checking");
        Assert.assertEquals(inputText, "Checking", "The input is not correct");
    }

    @Test(priority = 2)
    public void checkStateOneToAccount() {
        String inputText = transferFundsPage.readValueFromAccountToField();
        Log.assertion("The to account input field should have value Credit Card");
        Assert.assertEquals(inputText, "Credit Card", "The input is not correct");
    }

    @Test(priority = 2)
    public void checkStateOneAmount() {
        String inputText = transferFundsPage.readValueFromAmountField();
        Log.assertion("The amount input field should have value 300");
        Assert.assertEquals(inputText, "300", "The input is not correct");
    }

    @Test(priority = 2)
    public void checkStateOneDescription() {
        String inputText = transferFundsPage.readValueFromDescriptionField();
        Log.assertion("The description input field should have value Pay check for the UI Automation Course");
        Assert.assertEquals(inputText, "Pay check for the UI Automation Course", "The input is not correct");
    }

    @Test(priority = 3)
    public void checkStateTwoSuccessMessage() {
        transferFundsPage.clickContinue();
        String actualText = transferFundsPage.readSuccessMessageValue();
        Log.assertion("There should be success message that contains successfully key word");
        Assert.assertTrue(actualText.contains("successfully"), "There transaction wasn't successful");
    }

}
