import Utils.BaseTestClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class verifyTransferFundsFromSavingsToLoanIsWorkingCorrectlyImproved extends BaseTestClass {


    @Override
    protected void beforeClassExtended() {
        driver.get("http://zero.webappsecurity.com/");
        findElementAndClickIt(By.id("signin_button"));
        login("username", "password");
        navigateToTab("transfer_funds_tab");
        WebElement ddlFromAccount = driver.findElement(By.id("tf_fromAccountId"));
        chooseFromDDSOptionContainsText(ddlFromAccount, "Checking");
        WebElement ddlToAccount = driver.findElement(By.id("tf_toAccountId"));
        chooseFromDDSOptionContainsText(ddlToAccount, "Credit");
        findElementAndInsertText(By.cssSelector("input[name='amount']"), "300");
        findElementAndInsertText(By.id("tf_description"), "Pay check for the UI Automation Course");
        findElementAndClickIt(By.id("btn_submit"));
    }

    @Test(priority = 1)
    public void checkStateOneFromAccount() {
        checkTextValueOfInputElementLocatedById("tf_fromAccountId", "Checking");
    }

    @Test(priority = 1)
    public void checkStateOneToAccount() {
        checkTextValueOfInputElementLocatedById("tf_toAccountId", "Credit Card");
    }

    @Test(priority = 1)
    public void checkStateOneAmount() {
        checkTextValueOfInputElementLocatedById("tf_amount", "300");
    }

    @Test(priority = 1)
    public void checkStateOneDescription() {
        checkTextValueOfInputElementLocatedById("tf_description", "Pay check for the UI Automation Course");
    }

    @Test(priority = 2)
    public void checkStateTwoSuccessMessage() {
        findElementAndClickIt(By.id("btn_submit"));
        WebElement successMessageDiv = driver.findElement(By.cssSelector(".alert.alert-success"));
        String actualText = successMessageDiv.getText();
        Assert.assertTrue(actualText.contains("successfully"), "There transaction wasn't successful");
    }


    private void login(String user, String password) {
        WebElement inputUsername = driver.findElement(By.id("user_login"));
        inputUsername.sendKeys(user);
        WebElement inputPassword = driver.findElement(By.id("user_password"));
        inputPassword.sendKeys(password);
        WebElement btnSubmit = driver.findElement(By.className("btn-primary"));
        btnSubmit.click();
    }

    private void navigateToTab(String tabName) {
        WebElement linkTransferFunds = driver.findElement(By.id(tabName));
        linkTransferFunds.findElement(By.tagName("a")).click();
    }

    private void chooseFromDDSOptionContainsText(WebElement ddl, String itemText) {
        ddl.click();
        List<WebElement> ddlFromAccountsOptions = ddl.findElements(By.tagName("option"));
        for (WebElement ddlFromAccountsOption : ddlFromAccountsOptions) {
            String optionText = ddlFromAccountsOption.getText();
            if (optionText.contains(itemText)) {
                ddlFromAccountsOption.click();
                break;
            }
        }
        ddl.click();
    }

    private void findElementAndInsertText(By by, String text) {
        WebElement element = driver.findElement(by);
        element.clear();
        element.sendKeys(text);
    }

    private void findElementAndClickIt(By by) {
        WebElement element = driver.findElement(by);
        element.click();
    }

    private void checkTextValueOfInputElementLocatedById(String id, String expectedText) {
        WebElement inputFromAccount = driver.findElement(By.id(id));
        String inputText = inputFromAccount.getAttribute("value");
        Assert.assertEquals(inputText, expectedText, "The input is not correct");
    }

}
