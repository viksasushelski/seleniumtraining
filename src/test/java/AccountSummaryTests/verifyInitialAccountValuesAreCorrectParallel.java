package AccountSummaryTests;

import Data.DataProviders.AccountSummaryDataProvider;
import Data.DataReader;
import Data.ExcelParserService;
import Data.JsonParserService;
import Data.ResourcesPaths;
import Enums.AccountModelTypeEnum;
import Models.AccountModel;
import Pages.AccountSummaryPage;
import Pages.HomePage;
import Pages.LoginPage;
import Utils.BaseTestClass;
import Utils.ConfigurationConstants;
import Utils.TestAssertionUtil;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class verifyInitialAccountValuesAreCorrectParallel extends BaseTestClass {

    private HomePage homePage;
    private AccountSummaryPage accountSummaryPage;

    @Override
    protected void beforeClassExtended() throws IOException {
        homePage = new HomePage(driver);
        homePage = (HomePage) homePage.navigateTo(baseUrl, homePage);
        accountSummaryPage = homePage.clickBtnSignIn()
                .loginUsingCredentials(ConfigurationConstants.USERNAME_COURSE, ConfigurationConstants.PASSWORD_COURSE);
    }

    @Test
    public void checkAccountValues1() throws InterruptedException {
        AccountSummaryPage accountSummaryPage1 = accountSummaryPage.readCashAccounts();
//        accountSummaryPage.readCashAccounts();
        Thread.sleep(1000);
        List<AccountModel> accountModelListCash = accountSummaryPage1.getAccountModelListCash();
        accountModelListCash.add(AccountModel.builder().accountName("1").build());
        System.out.println(accountModelListCash.size());
        Assert.assertEquals(accountModelListCash.size(), 3);
    }

    @Test
    public void checkAccountValues2() throws InterruptedException {
        AccountSummaryPage accountSummaryPage2 = accountSummaryPage.readCashAccounts();
//        accountSummaryPage.readCashAccounts();
        Thread.sleep(1000);
        List<AccountModel> accountModelListCash = accountSummaryPage2.getAccountModelListCash();
        accountModelListCash.add(AccountModel.builder().accountName("2").build());
        System.out.println(accountModelListCash.size());
        Assert.assertEquals(accountModelListCash.size(), 3);
    }

    @Test
    public void checkAccountValues3() throws InterruptedException {
        AccountSummaryPage accountSummaryPage3 = accountSummaryPage.readCashAccounts();
//        accountSummaryPage.readCashAccounts();
        Thread.sleep(1000);
        List<AccountModel> accountModelListCash = accountSummaryPage3.getAccountModelListCash();
        accountModelListCash.add(AccountModel.builder().accountName("3").build());
        System.out.println(accountModelListCash.size());
        Assert.assertEquals(accountModelListCash.size(), 3);
    }

    @Test
    public void checkAccountValues4() throws InterruptedException {
        AccountSummaryPage accountSummaryPage4 = accountSummaryPage.readCashAccounts();
//        accountSummaryPage.readCashAccounts();
        Thread.sleep(1000);
        List<AccountModel> accountModelListCash = accountSummaryPage4.getAccountModelListCash();
        accountModelListCash.add(AccountModel.builder().accountName("4").build());
        System.out.println(accountModelListCash.size());
        Assert.assertEquals(accountModelListCash.size(), 3);
    }

}
