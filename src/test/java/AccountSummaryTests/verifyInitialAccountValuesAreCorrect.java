package AccountSummaryTests;

import Data.DataProviders.AccountSummaryDataProvider;
import Data.DataReader;
import Data.ExcelParserService;
import Data.JsonParserService;
import Data.ResourcesPaths;
import Enums.AccountModelTypeEnum;
import Models.AccountModel;
import Models.TestAssertModel;
import Pages.AccountSummaryPage;
import Pages.HomePage;
import Pages.LoginPage;
import Utils.BaseTestClass;
import Utils.ConfigurationConstants;
import Utils.TestAssertionUtil;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class verifyInitialAccountValuesAreCorrect extends BaseTestClass {

    private HomePage homePage;
    private LoginPage loginPage;
    private AccountSummaryPage accountSummaryPage;
    private DataReader dataReader;

    @Override
    protected void beforeClassExtended() throws IOException {
        homePage = new HomePage(driver);
        homePage = (HomePage) homePage.navigateTo(baseUrl, homePage);
        loginPage = homePage.clickBtnSignIn();
        accountSummaryPage = loginPage.loginUsingCredentials(ConfigurationConstants.USERNAME_COURSE, ConfigurationConstants.PASSWORD_COURSE);
    }

    @Test(priority = 1, dataProvider = "accountsTableDataProviderJson", dataProviderClass = AccountSummaryDataProvider.class)
    public void checkAccountValuesWithJson(AccountModelTypeEnum accountModelTypeEnum, String expectedResultsJson) {
        dataReader = new JsonParserService();
        accountSummaryPage.getAccountModelTypeEnumReadFunctionMap().get(accountModelTypeEnum).run();
        List<AccountModel> actualAccountList = accountSummaryPage.getAccountListByAccountTypeEnum(accountModelTypeEnum);
        List<AccountModel> expectedAccountList = dataReader.parseFromFileToListOfObject(ResourcesPaths.ACCOUNT_SUMMARY_TEST_DATA_PATH + expectedResultsJson, AccountModel.class);
        TestAssertionUtil.checkIfListsAreEqual(actualAccountList, expectedAccountList, softAssert);
        softAssert.assertAll();
    }

    @Test(priority = 1, dataProvider = "accountsTableDataProviderExcel", dataProviderClass = AccountSummaryDataProvider.class)
    public void checkAccountValuesWithExcel(AccountModelTypeEnum accountModelTypeEnum, String expectedResultsJson) {
        dataReader = new ExcelParserService("Sheet1");
        accountSummaryPage.getAccountModelTypeEnumReadFunctionMap().get(accountModelTypeEnum).run();
        List<AccountModel> actualAccountList = accountSummaryPage.getAccountListByAccountTypeEnum(accountModelTypeEnum);
        List<AccountModel> expectedAccountList = dataReader.parseFromFileToListOfObject(ResourcesPaths.ACCOUNT_SUMMARY_TEST_DATA_PATH + expectedResultsJson, AccountModel.class);
        TestAssertionUtil.checkIfListsAreEqual(actualAccountList, expectedAccountList, softAssert);
        softAssert.assertAll();
    }

    @Test(priority = 1, dataProvider = "accountsTableDataProviderInCode", dataProviderClass = AccountSummaryDataProvider.class)
    public void checkCashAccountValues(AccountModel accountModel) {
        accountSummaryPage.readCashAccounts();
        List<AccountModel> actualAccountList = accountSummaryPage.getAccountModelListCash();
        Assert.assertTrue(actualAccountList.contains(accountModel), "The account is not in the list");
    }


    public void checkCashAccountValuesLargerThanLimit(AccountModel accountModel) {
        accountSummaryPage = accountSummaryPage.readCashAccounts();
        TestAssertModel testAssertModel = accountSummaryPage.checkIfAllCashAccountHasHigherValueThan(100);
        Assert.assertTrue(testAssertModel.isPassed(), testAssertModel.getErrorMessage());
    }

    @Parameters("param")
    @Test(priority = 2)
    public void testWithParameter(String param) {
        Assert.assertEquals(param, "something");
    }


}
