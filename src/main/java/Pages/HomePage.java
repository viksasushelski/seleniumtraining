package Pages;

import Utils.BasePage;
import Utils.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(id = "signin_button")
    private WebElement btnSignIn;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        executeCurrentMethodLog();
        return new HomePage(driver);
    }

    public LoginPage clickBtnSignIn() {
        executeCurrentMethodLog();
        waitForElementToBeClickableAndClick(btnSignIn);
        Log.stepsToReproduce("User has clicked the sign in button");
        return new LoginPage(getDriver());
    }
}
