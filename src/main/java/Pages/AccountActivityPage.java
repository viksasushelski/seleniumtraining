package Pages;

import Utils.BasePage;
import org.openqa.selenium.WebDriver;

public class AccountActivityPage extends BasePage {

    public AccountActivityPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        executeCurrentMethodLog();
        return new AccountActivityPage(driver);
    }
}
