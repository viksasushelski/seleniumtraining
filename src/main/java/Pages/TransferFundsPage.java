package Pages;

import Components.SimpleDropDownComponent;
import Enums.DropDownsEnum;
import Enums.TransferFundsAccountTypesEnum;
import Features.DropDown.DropDownFeature;
import Features.DropDown.SimpleDropDown;
import Models.TestAssertModel;
import Utils.BasePage;
import Utils.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class TransferFundsPage extends BasePage {

    public static final String ALERT_SUCCESS_MESSAGE_CSS_LOCATOR = ".alert.alert-success";
    @FindBy(id = "tf_fromAccountId")
    private WebElement ddlFromAccount;

    @FindBy(id = "tf_toAccountId")
    private WebElement ddlToAccount;

    @FindBy(id = "tf_amount")
    private WebElement inputAmount;

    @FindBy(id = "tf_description")
    private WebElement inputDescription;

    @FindBy(id = "btn_submit")
    private WebElement btnSubmit;

    private SimpleDropDownComponent fromAccountDropDownComponent;
    private SimpleDropDownComponent toAccountDropDownComponent;

    public TransferFundsPage(WebDriver driver) {
        super(driver);
        DropDownFeature dropDownFeature = new SimpleDropDown(getDriver());
        fromAccountDropDownComponent = new SimpleDropDownComponent(ddlFromAccount, dropDownFeature);
        toAccountDropDownComponent = new SimpleDropDownComponent(ddlToAccount, dropDownFeature);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        return new TransferFundsPage(driver);
    }

    public TransferFundsPage chooseFromAccount(TransferFundsAccountTypesEnum fromAccount) {
        fromAccountDropDownComponent.chooseFromDDL(fromAccount);
        Log.stepsToReproduce("User has chosen: " + fromAccount.toString() + " from the FromAccount ddl");
        return new TransferFundsPage(this.getDriver());
    }

    public TransferFundsPage chooseToAccount(TransferFundsAccountTypesEnum toAccount) {
        toAccountDropDownComponent.chooseFromDDL(toAccount);
        Log.stepsToReproduce("User has chosen: " + toAccount.toString() + " from the ToAccount ddl");
        return new TransferFundsPage(this.getDriver());
    }

    public TransferFundsPage insertAmountValue(String amount) {
        executeCurrentMethodLog();
        clearAndSendKeys(inputAmount, amount);
        Log.stepsToReproduce("User has inserted: " + amount + " into the Amount field");
        return new TransferFundsPage(this.getDriver());
    }

    public TransferFundsPage insertDescription(String description) {
        executeCurrentMethodLog();
        clearAndSendKeys(inputDescription, description);
        Log.stepsToReproduce("User has inserted: " + description + " into the Description field");
        return new TransferFundsPage(this.getDriver());
    }

    public TransferFundsPage clickContinue() {
        executeCurrentMethodLog();
        btnSubmit.click();
        Log.stepsToReproduce("User has clicked continue");
        return new TransferFundsPage(this.getDriver());
    }

    public TransferFundsPage insertAllValuesAndClickContinue(TransferFundsAccountTypesEnum fromAccount, TransferFundsAccountTypesEnum toAccount, String amount, String description) {
        fromAccountDropDownComponent.chooseFromDDL(fromAccount);
        toAccountDropDownComponent.chooseFromDDL(toAccount);
        insertAmountValue(amount);
        insertDescription(description);
        clickContinue();
        return new TransferFundsPage(this.getDriver());
    }

    private void chooseFromDDSOptionContainsText(WebElement ddl, String itemText) {
        executeCurrentMethodLog();
        ddl.click();
        List<WebElement> ddlFromAccountsOptions = waitAndFindElements(ddl, By.tagName("option"));
        for (WebElement ddlFromAccountsOption : ddlFromAccountsOptions) {
            String optionText = ddlFromAccountsOption.getText();
            if (optionText.contains(itemText)) {
                ddlFromAccountsOption.click();
                break;
            }
        }
        ddl.click();
    }

    public String readValueFromAccountFromField() {
        return getValueFromInputField(ddlFromAccount);
    }

    public String readValueFromAccountToField() {
        executeCurrentMethodLog();
        return getValueFromInputField(ddlToAccount);
    }

    public String readValueFromAmountField() {
        executeCurrentMethodLog();
        return getValueFromInputField(inputAmount);
    }

    public String readValueFromDescriptionField() {
        executeCurrentMethodLog();
        return getValueFromInputField(inputDescription);
    }

    private String getValueFromInputField(WebElement inputField) {
        executeCurrentMethodLog();
        return inputField.getAttribute("value");
    }

    public String readSuccessMessageValue() {
        executeCurrentMethodLog();
        WebElement successMessageDiv = waitAndFindElementFromRoot(By.cssSelector(ALERT_SUCCESS_MESSAGE_CSS_LOCATOR));
        return successMessageDiv.getText();
    }

    public TestAssertModel checkDDlContainsValues(DropDownsEnum dropDownsEnum, String... optionsTexts) {
        if (dropDownsEnum.equals(DropDownsEnum.FROM_ACCOUNT))
            return fromAccountDropDownComponent.checkDDlContainsValues(optionsTexts);
        if (dropDownsEnum.equals(DropDownsEnum.TO_ACCOUNT))
            return toAccountDropDownComponent.checkDDlContainsValues(optionsTexts);

        throw new RuntimeException("There is no implementation for this drop down: " + dropDownsEnum);
    }
}
