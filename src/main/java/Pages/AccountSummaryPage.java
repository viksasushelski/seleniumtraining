package Pages;

import Enums.AccountModelTypeEnum;
import Enums.HeaderMenuItemsEnum;
import Models.AccountModel;
import Models.TestAssertModel;
import Utils.BasePage;
import Utils.Log;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class AccountSummaryPage extends BasePage {

    private List<AccountModel> accountModelListCash;
    private List<AccountModel> accountModelListCredit;
    private List<AccountModel> accountModelListInvestment;
    private List<AccountModel> accountModelListLoan;
    private Map<AccountModelTypeEnum, Runnable> accountModelTypeEnumReadFunctionMap;

    @FindBy(className = "table")
    private List<WebElement> tablesContainers;

    public AccountSummaryPage(WebDriver driver) {
        super(driver);
        populateReadFunctionsMap();
        accountModelListCash = new ArrayList<>();
        accountModelListCredit = new ArrayList<>();
        accountModelListInvestment = new ArrayList<>();
        accountModelListLoan = new ArrayList<>();
    }

    public AccountSummaryPage(AccountSummaryPage other) {
        super(other.getDriver());
        accountModelListCash = new ArrayList<>(other.getAccountModelListCash());
        accountModelListCredit = new ArrayList<>(other.getAccountModelListCredit());
        accountModelListInvestment = new ArrayList<>(other.getAccountModelListInvestment());
        accountModelListLoan = new ArrayList<>(other.getAccountModelListLoan());
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        executeCurrentMethodLog();
        return new AccountSummaryPage(driver);
    }

    public TransferFundsPage navigateToTransferFunds() {
        executeCurrentMethodLog();
        WebElement linkContainer = waitAndFindElementFromRoot(By.id(HeaderMenuItemsEnum.TRANSFER_FUNDS.toString()));
        WebElement linkButton = waitAndFindElement(linkContainer, By.tagName("a"));
        waitForElementToBeClickableAndClick(linkButton);
        writeDriversLogs();
        Log.stepsToReproduce("User has navigated to Transfer Funds Screen");
        return new TransferFundsPage(getDriver());
    }

    public AccountSummaryPage readCashAccounts() {
        accountModelListCash = readAccountModelTable(tablesContainers.get(0));
        return new AccountSummaryPage(this);
    }

//    public void readCashAccounts() {
//        accountModelListCash = readAccountModelTable(tablesContainers.get(0));
//    }

    public AccountSummaryPage readInvestmentAccounts() {
        accountModelListInvestment = readAccountModelTable(tablesContainers.get(1));
        return new AccountSummaryPage(this);
    }

    public AccountSummaryPage readCreditAccounts() {
        accountModelListCredit = readAccountModelTable(tablesContainers.get(2));
        return new AccountSummaryPage(this);
    }

    public AccountSummaryPage readLoanAccounts() {
        accountModelListLoan = readAccountModelTable(tablesContainers.get(3));
        return new AccountSummaryPage(this);
    }

    private List<AccountModel> readAccountModelTable(WebElement table) {
        executeCurrentMethodLog();
        List<AccountModel> accountModelList = new ArrayList<>();
        WebElement tableBody = waitAndFindElement(table, By.tagName("tbody"));
        List<WebElement> tableRows = waitAndFindElements(tableBody, By.xpath("tr"));
        for (WebElement tableRow : tableRows) {
            accountModelList.add(readAccountModelRow(tableRow));
        }
        return accountModelList;
    }

    private AccountModel readAccountModelRow(WebElement tableRow) {
        executeCurrentMethodLog();
        Optional<WebElement> accountColumn = findColumnByLocator(tableRow, By.cssSelector("td:nth-child(1)"));
        Optional<WebElement> creditCardColumn = findColumnByLocator(tableRow, By.cssSelector("td:nth-child(2)"));
        Optional<WebElement> balanceColumn = findColumnByLocator(tableRow, By.cssSelector("td:nth-child(3)"));
        return AccountModel.builder()
                .accountLink(accountColumn.isPresent() ? waitAndFindElement(accountColumn.get(), By.tagName("a")) : null)
                .accountName(accountColumn.isPresent() ? waitAndFindElement(accountColumn.get(), By.tagName("a")).getText() : null)
                .creditCardNumber(creditCardColumn.isPresent() ? creditCardColumn.get().getText() : null)
                .balance(balanceColumn.isPresent() ? balanceColumn.get().getText() : null)
                .build();
    }

    private Optional<WebElement> findColumnByLocator(WebElement row, By by) {
        return checkAndGetIfElementIsPresent(row, by);
    }

    public AccountActivityPage clickAccountByNameAndModel(String name, AccountModelTypeEnum accountModelTypeEnum) {
        executeCurrentMethodLog();
        //todo implement logic
        return null;
    }

    public Optional<AccountModel> findAccountByNameAndModel(String name, AccountModelTypeEnum accountModelTypeEnum) {
        executeCurrentMethodLog();
        if (accountModelTypeEnum == AccountModelTypeEnum.CASH) {
            if (accountModelListCash.isEmpty()) {
                //todo handle this
            }
            return findAccountByName(name, accountModelListCash);
        }
        if (accountModelTypeEnum == AccountModelTypeEnum.INVESTMENT) {
            return findAccountByName(name, accountModelListInvestment);
        }
        if (accountModelTypeEnum == AccountModelTypeEnum.CREDIT) {
            return findAccountByName(name, accountModelListCredit);
        }
        if (accountModelTypeEnum == AccountModelTypeEnum.LOAN) {
            return findAccountByName(name, accountModelListLoan);
        }
        throw new RuntimeException("The enum doesn't include some list");
    }

    private Optional<AccountModel> findAccountByName(String accountName, List<AccountModel> accountModelList) {
        executeCurrentMethodLog();
        return accountModelList.stream().filter(accountModel -> accountName.equals(accountModel.getAccountName()))
                .findFirst();
    }

    public List<AccountModel> getAccountListByAccountTypeEnum(AccountModelTypeEnum accountModelTypeEnum) {
        executeCurrentMethodLog();
        if (accountModelTypeEnum == AccountModelTypeEnum.CASH) {
            return accountModelListCash;
        }
        if (accountModelTypeEnum == AccountModelTypeEnum.INVESTMENT) {
            return accountModelListInvestment;
        }
        if (accountModelTypeEnum == AccountModelTypeEnum.CREDIT) {
            return accountModelListCredit;
        }
        if (accountModelTypeEnum == AccountModelTypeEnum.LOAN) {
            return accountModelListLoan;
        }
        throw new RuntimeException("Not defined enum");
    }

    private void populateReadFunctionsMap() {
        accountModelTypeEnumReadFunctionMap = new HashMap<>();
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.CASH, this::readCashAccounts);
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.INVESTMENT, this::readInvestmentAccounts);
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.CREDIT, this::readCreditAccounts);
        accountModelTypeEnumReadFunctionMap.put(AccountModelTypeEnum.LOAN, this::readLoanAccounts);

    }

    public TestAssertModel checkIfAllCashAccountHasHigherValueThan(double limitValue) {
        if (accountModelListCash.isEmpty()) {
            return TestAssertModel.builder()
                    .passed(false)
                    .errorMessage("Abe idi procitaj si ja listata")
                    .build();
        }
        String errorMessage = null;
        boolean allMatch = accountModelListCash.stream()
                .allMatch(accountModel -> limitValue < Double.valueOf(accountModel.getBalance()));
        if (!allMatch) {
            List<String> accountsWithLowerValues = accountModelListCash.stream()
                    .filter(accountModel -> limitValue > Double.valueOf(accountModel.getBalance()))
                    .collect(Collectors.toList())
                    .stream()
                    .map(AccountModel::getAccountName)
                    .collect(Collectors.toList());
            StringBuilder stringBuilder = new StringBuilder();
            for (String accountsWithLowerValue : accountsWithLowerValues) {
                stringBuilder.append(accountsWithLowerValue).append("||");
            }
            errorMessage = stringBuilder.toString();
        }
        return TestAssertModel.builder()
                .passed(allMatch)
                .errorMessage(allMatch ? null : errorMessage)
                .build();
    }


}
