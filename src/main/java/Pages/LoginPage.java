package Pages;

import Utils.BasePage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(id = "user_login")
    private WebElement inputLogin;

    @FindBy(id = "user_password")
    private WebElement inputPassword;

    @FindBy(id = "user_remember_me")
    private WebElement checkBoxRememberMe;

    @FindBy(className = "btn-primary")
    private WebElement btnSignIn;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public BasePage newInstance(WebDriver driver) {
        executeCurrentMethodLog();
        return new LoginPage(driver);
    }

    public LoginPage insertUserName(String userName) {
        executeCurrentMethodLog();
        clearAndSendKeys(inputLogin, userName);
        return new LoginPage(this.getDriver());
    }

    public LoginPage insertPassword(String password) {
        executeCurrentMethodLog();
        clearAndSendKeys(inputPassword, password);
        return new LoginPage(this.getDriver());
    }

    public LoginPage checkRememeberMe() {
        executeCurrentMethodLog();
        checkBoxRememberMe.click();
        return new LoginPage(this.getDriver());
    }

    public LoginPage clickSignIn() {
        executeCurrentMethodLog();
        btnSignIn.click();
        return new LoginPage(this.getDriver());
    }

    public AccountSummaryPage loginUsingCredentials(String userName, String password) {
        insertUserName(userName);
        insertPassword(password);
        clickSignIn();
        writeDriversLogs();
        return new AccountSummaryPage(getDriver());
    }
}
