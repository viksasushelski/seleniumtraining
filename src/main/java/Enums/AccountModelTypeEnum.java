package Enums;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public enum AccountModelTypeEnum {

    CASH,
    INVESTMENT,
    CREDIT,
    LOAN;
}
