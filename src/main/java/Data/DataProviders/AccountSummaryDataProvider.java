package Data.DataProviders;

import Enums.AccountModelTypeEnum;
import Models.AccountModel;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class AccountSummaryDataProvider {

    @DataProvider(name = "accountsTableDataProviderJson")
    public static Object[][] populateAccountProviderWithJsonExpected() {
        return new Object[][]{
                {AccountModelTypeEnum.CASH, "expectedCashAccounts.json"},
                {AccountModelTypeEnum.INVESTMENT, "expectedInvestmentAccounts.json"},
                {AccountModelTypeEnum.CREDIT, "expectedCreditAccounts.json"},
                {AccountModelTypeEnum.LOAN, "expectedLoanAccounts.json"},
        };
    }

    @DataProvider(name = "accountsTableDataProviderExcel")
    public static Object[][] populateAccountProviderWithExcelExpected() {
        return new Object[][]{
                {AccountModelTypeEnum.CREDIT, "expectedCreditAccounts.xls"},
        };
    }

    @DataProvider(name = "accountsTableDataProviderInCode")
    public static Object[][] populateAccountProviderInCode() {
        List<AccountModel> accountModelList = populateSavingsAccountExpectedList();
        Object[][] objArray = new Object[accountModelList.size()][];
        for (int i = 0; i < accountModelList.size(); i++) {
            objArray[i] = new Object[1];
            objArray[i][0] = accountModelList.get(i);
        }
        return objArray;
    }

    private static List<AccountModel> populateSavingsAccountExpectedList() {
        List<AccountModel> accountModelList = new ArrayList<>();
        accountModelList.add(AccountModel.builder().accountName("Savings").creditCardNumber("").balance("$1000").build());
        accountModelList.add(AccountModel.builder().accountName("Savings").creditCardNumber("").balance("$1000").build());
        return accountModelList;
    }

}
