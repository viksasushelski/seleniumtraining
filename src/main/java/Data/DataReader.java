package Data;

import java.util.List;

public interface DataReader {

    <E> List<E> parseFromFileToListOfObject(String path, Class<?> typeClass);

}
