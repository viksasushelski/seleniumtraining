package Data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by vsushelski on 8/13/2017.
 */
public class JsonParserService implements DataReader {

    static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    @Override
    public <E> List<E> parseFromFileToListOfObject(String path, Class<?> typeClass) {
        try {
            return objectMapper.readValue(new File(path),
                    TypeFactory.defaultInstance().constructCollectionType(List.class, typeClass));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String getJsonStringFromListOfObjects(List<?> objectList) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(objectList);
            return json;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }


}
