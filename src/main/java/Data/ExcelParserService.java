package Data;

import Models.AccountModel;

import java.util.ArrayList;
import java.util.List;

public class ExcelParserService implements DataReader {

    private Excel excel;
    private String sheetName;

    private ExcelParserService() {
    }

    public ExcelParserService(String sheetName) {
        this.sheetName = sheetName;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <E> List<E> parseFromFileToListOfObject(String path, Class<?> typeClass) {
        excel = new Excel(path, sheetName);
        return (List<E>) readFromExcelRowIntoAccountModel();
    }

    private List<AccountModel> readFromExcelRowIntoAccountModel() {
        List<AccountModel> accountModelList = new ArrayList<>();
        for (int i = 0; i <= excel.getLastRow(); i++) {
            AccountModel account = AccountModel.builder()
                    .accountName(excel.getCell(i, 0))
                    .creditCardNumber(excel.getCell(i, 1))
                    .balance(excel.getCell(i, 2))
                    .build();
            accountModelList.add(account);
        }
        return accountModelList;
    }
}
