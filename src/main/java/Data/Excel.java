package Data;

/**
 * Created by dvelinovska on 9/14/2016.
 */

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class Excel {

    private Workbook wb;
    private Sheet ws;

    public Excel(String fileName, String sheetName) {
        try {
            if (!fileName.contains("xlsx")) {
                wb = new HSSFWorkbook(new FileInputStream(new File(fileName)));
                ws = wb.getSheet(sheetName);
            } else {
                wb = new XSSFWorkbook(fileName);
                ws = wb.getSheet(sheetName);
            }
        } catch (IOException io) {
            System.err.println("Invalid file '" + fileName
                    + "' or incorrect sheet '" + sheetName
                    + "', enter a valid one");
        }
    }

    public String getCell(int rowIndex, int columnIndex) {
        Cell cell = null;
        try {
            cell = ws.getRow(rowIndex).getCell(columnIndex);
        } catch (Exception e) {
            System.err.println("The cell with row '" + rowIndex + "' and column '"
                    + columnIndex + "' doesn't exist in the sheet");
        }
        return new DataFormatter().formatCellValue(cell);
    }

    public int getLastRow() {
        return ws.getLastRowNum();
    }


}

