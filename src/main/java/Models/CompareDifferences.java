package Models;

public interface CompareDifferences<T> {

     String compareAndPrintAllDifferences(T o);
}
