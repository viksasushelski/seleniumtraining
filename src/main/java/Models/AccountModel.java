package Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.openqa.selenium.WebElement;

import java.util.Comparator;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class AccountModel implements Comparable<AccountModel>, CompareDifferences<AccountModel> {

    private String accountName;
    private String creditCardNumber;
    private String balance;

    @JsonIgnore
    private WebElement accountLink;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountModel that = (AccountModel) o;

        if (!accountName.equals(that.accountName)) return false;
        if (creditCardNumber != null ? !creditCardNumber.equals(that.creditCardNumber) : that.creditCardNumber != null)
            return false;
        return balance.equals(that.balance);
    }

    @Override
    public int hashCode() {
        int result = accountName.hashCode();
        result = 31 * result + (creditCardNumber != null ? creditCardNumber.hashCode() : 0);
        result = 31 * result + balance.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AccountModel{" +
                "accountName='" + accountName + '\'' +
                ", creditCardNumber='" + creditCardNumber + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }

    @Override
    public int compareTo(AccountModel o) {
        return Comparator.comparing(AccountModel::getAccountName)
                .thenComparing(AccountModel::getBalance)
                .compare(this, o);
    }

    @Override
    public String compareAndPrintAllDifferences(AccountModel actualAccount) {
        StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("There is difference for this account : ").append(actualAccount.toString()).append("\n");

        if (!accountName.equals(actualAccount.accountName)) {
            errorMessage.append("Account name is different. It is expected to be: ").append(accountName).append("\n");
        }
        if (!creditCardNumber.equals(actualAccount.creditCardNumber)) {
            errorMessage.append("Credit card is different. It is expected to be: ").append(creditCardNumber).append("\n");
        }
        if (!balance.equals(actualAccount.balance)) {
            errorMessage.append("Balance is different. It is expected to be: ").append(balance).append("\n");
        }
        return errorMessage.toString();
    }
}
