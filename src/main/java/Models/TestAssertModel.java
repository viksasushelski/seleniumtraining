package Models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TestAssertModel {

    private boolean passed;
    private String errorMessage;
}
