package Utils;

import Pages.AccountSummaryPage;
import Pages.HomePage;
import org.openqa.selenium.WebDriver;

public class TestFlowUitls {

    public static AccountSummaryPage navigateToBaseUrlAndLogin(WebDriver driver, String baseUrl, String userName, String password) {
        HomePage homePage = new HomePage(driver);
        homePage = (HomePage) homePage.navigateTo(baseUrl, homePage);
        return homePage.clickBtnSignIn()
                .loginUsingCredentials(ConfigurationConstants.USERNAME_COURSE, ConfigurationConstants.PASSWORD_COURSE);
    }

}
