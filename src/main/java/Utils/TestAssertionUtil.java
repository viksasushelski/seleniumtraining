package Utils;

import Models.CompareDifferences;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.stream.Collectors;

public class TestAssertionUtil {

    @SuppressWarnings("unchecked")
    public static void checkIfListsAreEqual(List<? extends CompareDifferences> actual, List<? extends CompareDifferences> expected, SoftAssert softAssert) {
        Assert.assertEquals(actual.size(), expected.size(), "There is difference in lists size");
        actual = sortListByComparator(actual);
        expected = sortListByComparator(expected);
        for (int i = 0; i < expected.size(); i++) {
            softAssert.assertEquals(actual.get(i), expected.get(i), expected.get(i).compareAndPrintAllDifferences(actual.get(i)));
        }
    }

    private static <T> List<T> sortListByComparator(List<T> unSortedList) {
        return unSortedList.stream()
                .sorted()
                .collect(Collectors.toList());
    }

}
