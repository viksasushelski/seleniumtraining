package Utils;

import Enums.DriverTypeEnum;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.logging.Level;

public class DriverFactory {

    private final static String GOOGLE_CHROME_SET_UP_NAME = "webdriver.chrome.driver";
    private final static String GOOGLE_CHROME_SET_UP_PATH = "src/main/resources/Drivers/chromedriver.exe";

    private final static String FIREFOX_SET_UP_NAME = "webdriver.gecko.driver";
    private final static String FIREFOX_SET_UP_PATH = "src/main/resources/Drivers/geckodriver.exe";

    public static WebDriver createDriverForBrowserWithValue(DriverTypeEnum driverType) {

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        logPrefs.enable(LogType.CLIENT, Level.ALL);
        logPrefs.enable(LogType.DRIVER, Level.ALL);
        logPrefs.enable(LogType.PROFILER, Level.ALL);
        desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        WebDriver driver = null;

        if (driverType.equals(DriverTypeEnum.GOOGLE_CHROME_DRIVER)) {
            System.setProperty(GOOGLE_CHROME_SET_UP_NAME, GOOGLE_CHROME_SET_UP_PATH);
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.merge(desiredCapabilities);
            driver = new ChromeDriver(chromeOptions);
        }
        if (driverType.equals(DriverTypeEnum.GOOGLE_CHROME_DRIVER_HEADLESS)) {
            System.setProperty(GOOGLE_CHROME_SET_UP_NAME, GOOGLE_CHROME_SET_UP_PATH);
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("window-size=1920x1080");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--allow-insecure-localhost");
            chromeOptions.merge(desiredCapabilities);
            driver = new ChromeDriver(chromeOptions);
        }
        if (driverType.equals(DriverTypeEnum.FIRE_FOX_DRIVER)) {
            System.setProperty(FIREFOX_SET_UP_NAME, FIREFOX_SET_UP_PATH);
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.merge(desiredCapabilities);
            driver = new FirefoxDriver();
        }
        if (driver == null) {
            throw new RuntimeException("The driver wasn't initialised");
        }
        driver.manage().window().maximize();
        return driver;
    }
}
