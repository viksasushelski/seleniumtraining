package Utils;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

@Getter
public abstract class BasePage extends BaseSelenium {

    public BasePage(WebDriver driver) {
        super(driver);
        writeDriversLogs();
        //handle loading
        //remove header
        //check for success messages
        //check for error messages
    }

    public abstract BasePage newInstance(WebDriver driver);

    public <T extends BasePage> BasePage navigateTo(String url, T type) {
        getDriver().get(url);
        writeDriversLogs();
        Log.stepsToReproduce("User has navigated to url: " + url);
        return type.newInstance(getDriver());
    }

    protected void executeCurrentMethodLog() {
        String className = Thread.currentThread().getStackTrace()[2].getClassName();
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        Log.info("Class Name : " + className + "-Executing Method : " + methodName);
    }
}
