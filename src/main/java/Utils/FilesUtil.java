package Utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class FilesUtil {

    public static void writeStepsToReproduceToFile(String text, String name) throws IOException {
        List<String> steps = Arrays.asList(text.split("\n"));
        Path file = Paths.get(name + ".txt");
        Files.write(file, steps, Charset.forName("UTF-8"));
    }

}
