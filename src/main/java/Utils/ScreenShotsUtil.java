package Utils;

import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class ScreenShotsUtil {

    public static void saveScreenShoot(ITestResult result, File scrFile, String baseUrl, String pictureName) {
        String fileName = "target/surefire-reports/" + pictureName + ".png";
        fileName = fileName.replaceAll(":", "-");
        if (!result.isSuccess()) {
            Log.info("Taking screenshots for failed test with name: " + result.getTestName());
            File destFile = new File(fileName);
            try {
                FileUtils.copyFile(scrFile, destFile);
            } catch (IOException e) {
                Log.error("Screenshot failed");
            }
            Object[] objects = new Object[1];
            objects[0] = generateTestInfo(baseUrl, "No Version", destFile.getAbsolutePath());
            result.setParameters(objects);
        } else {
            try {
                Path path = Paths.get(fileName);
                Files.delete(path);
            } catch (NoSuchFileException x) {
                System.err.format("%s: no such" + " file or directory%n", fileName);
            } catch (DirectoryNotEmptyException x) {
                System.err.format("%s not empty%n", fileName);
            } catch (IOException x) {
                System.err.println(x);
            }
        }
    }

    private static String generateTestInfo(String environment, String version, String screenshotPath) {
        return "\nEnvironment: " + environment +
                "\nApplication version: " + version +
                "\nFailed test screenshot path:\n" + screenshotPath;
    }

}
