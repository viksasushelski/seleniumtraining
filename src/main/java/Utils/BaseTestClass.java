package Utils;

import Enums.DriverTypeEnum;
import org.apache.tools.ant.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class BaseTestClass extends BaseTest {

    public static final int SKIPED_TEST_STATUS = 3;
    public static final int FAILED_TEST_STATUS = 2;
    public static final String STEPS_TO_REPRODUCE_FILE_PATH = "src/test/TestsStepsToReproduce/";
    protected WebDriver driver;
    protected String baseUrl;
    protected SoftAssert softAssert;
    protected File scrFile;

    @BeforeClass
    protected void beforeClass() throws IOException {
        Log.stepsToReproduce("Before has started for test with name: " + this.getClass().getName());
        baseUrl = ProperitiesReader.readFromProperties(ConfigurationConstants.MY_PROPERTIES_PATH, ConfigurationConstants.BASE_URL_PROPERTY);
        String driverType = ProperitiesReader.readFromProperties(ConfigurationConstants.MY_PROPERTIES_PATH, ConfigurationConstants.DRIVER_TYPE_PROPERTY);
        driver = DriverFactory.createDriverForBrowserWithValue(DriverTypeEnum.parse(driverType));
        beforeClassExtended();
        Log.stepsToReproduce("Before has ended for test with name: " + this.getClass().getName());
    }

    protected void beforeClassExtended() throws IOException {
    }

    @BeforeMethod
    protected void beforeMethod() {
        softAssert = new SoftAssert();
//        Log.startTestCase();
    }

    @AfterMethod
    protected void afterMethod(ITestResult result) {
        String methodName = result.getMethod().getMethodName();
        Log.endTestCase(methodName);
        takeScreenShoot();
        saveScreenShoot(result);
    }

    protected void afterClassExtended() {
    }

    @AfterClass(alwaysRun = true)
    protected void afterClass(ITestContext context) {
        try {
            afterClassExtended();
            String parallel = context.getSuite().getParallel();
            if (parallel.equals("false")) {
                FilesUtil.writeStepsToReproduceToFile(Log.getStepsLog().toString(), STEPS_TO_REPRODUCE_FILE_PATH + this.getClass().getName());
                Log.cleanTestSuitLog();
                FilesUtil.writeStepsToReproduceToFile(Log.getConsoleLog().toString(), STEPS_TO_REPRODUCE_FILE_PATH + "ConsoleLog/" + this.getClass().getName());
                Log.cleanConsoleLog();
                FilesUtil.writeStepsToReproduceToFile(Log.getNetworkLogs().toString(), STEPS_TO_REPRODUCE_FILE_PATH + "NetworkLog/" + this.getClass().getName());
                Log.cleanNetworkLog();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            driver.close();
            driver.quit();
            Log.info("Test has ended");
            Log.stepsToReproduce("Test class has ended :" + this.getClass().getName());
        }
    }

    private void takeScreenShoot() {
        scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    }

    private void saveScreenShoot(ITestResult result) {
        String pictureName = null;

        String suiteName = result.getTestContext().getSuite().getName();
        if (result.getStatus() == SKIPED_TEST_STATUS) {
            pictureName = suiteName + "/" + this.getClass().getName() + "--Before";
        }
        if (result.getStatus() == FAILED_TEST_STATUS) {
            String methodName = result.getMethod().getMethodName();
            pictureName = suiteName + "/" + this.getClass().getName() + "---" + methodName;
        }
        ScreenShotsUtil.saveScreenShoot(result, scrFile, baseUrl, pictureName);
    }

}
